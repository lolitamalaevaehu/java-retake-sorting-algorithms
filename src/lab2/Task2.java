package lab2;

public class Task2 {
    // Selection Sort
    public static void selectionSort(String[] arr) {
        for (int i = 0; i < arr.length - 1; i++) {
            int minIndex = i;
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[j].compareTo(arr[minIndex]) < 0) {
                    minIndex = j;
                }
            }
            String temp = arr[minIndex];
            arr[minIndex] = arr[i];
            arr[i] = temp;
        }
    }

    // Bubble Sort
    public static void bubbleSort(String[] arr) {
        for (int i = 0; i < arr.length - 1; i++) {
            for (int j = 0; j < arr.length - i - 1; j++) {
                if (arr[j].compareTo(arr[j + 1]) > 0) {
                    String temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                }
            }
        }
    }

    // Insertion Sort
    public static void insertionSort(String[] arr) {
        for (int i = 1; i < arr.length; i++) {
            String current = arr[i];
            int j = i - 1;
            while (j >= 0 && arr[j].compareTo(current) > 0) {
                arr[j + 1] = arr[j];
                j--;
            }
            arr[j + 1] = current;
        }
    }

    public static void main(String[] args) {
        String[] array = {"Ivanov", "Petrov", "Sidorov", "Ivanov"};
        System.out.println("Original Array: " + String.join(", ", array));

        // Selection Sort
        String[] selectionSortedArray = array.clone();
        selectionSort(selectionSortedArray);
        System.out.println("Selection Sorted: " + String.join(", ", selectionSortedArray));

        // Bubble Sort
        String[] bubbleSortedArray = array.clone();
        bubbleSort(bubbleSortedArray);
        System.out.println("Bubble Sorted: " + String.join(", ", bubbleSortedArray));

        // Insertion Sort
        String[] insertionSortedArray = array.clone();
        insertionSort(insertionSortedArray);
        System.out.println("Insertion Sorted: " + String.join(", ", insertionSortedArray));
    }
}
