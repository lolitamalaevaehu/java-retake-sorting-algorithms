package lab3;

public class Task3 {

    public static void main(String[] args) {
        String[] names = {"Egor", "Artiom", "Egor", "Vlad", "Igor", "Ivan", "Kirill"};
        String[] surnames = {"Anufriev", "Abikenov", "Kalinchuk", "Ilyin", "Loginov", "Minin", "Fomichev"};

        insertionSort(names, surnames);


        System.out.println("Sorted Names:");
        for (String name : names) {
            System.out.println(name);
        }

        System.out.println("\nSorted Surnames:");
        for (String surname : surnames) {
            System.out.println(surname);
        }
    }

    private static void insertionSort(String[] names, String[] surnames) {
        for (int i = 1; i < names.length; i++) {
            String keyName = names[i];
            String keySurname = surnames[i];
            int j = i - 1;

            while (j >= 0 && (names[j].compareTo(keyName) > 0 ||
                    (names[j].equals(keyName) && surnames[j].compareTo(keySurname) > 0))) {
                names[j + 1] = names[j];
                surnames[j + 1] = surnames[j];
                j = j - 1;
            }
            names[j + 1] = keyName;
            surnames[j + 1] = keySurname;
        }
    }
}
