package lab1;

public class Task1 {

    // Selection Sort
    public static void selectionSort(char[] arr) {
        for (int i = 0; i < arr.length - 1; i++) {
            int minIndex = i;
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[j] < arr[minIndex]) {
                    minIndex = j;
                }
            }
            char temp = arr[minIndex];
            arr[minIndex] = arr[i];
            arr[i] = temp;
        }
    }

    // Bubble Sort
    public static void bubbleSort(char[] arr) {
        for (int i = 0; i < arr.length - 1; i++) {
            for (int j = 0; j < arr.length - i - 1; j++) {
                if (arr[j] > arr[j + 1]) {
                    char temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                }
            }
        }
    }

    // Insertion Sort
    public static void insertionSort(char[] arr) {
        for (int i = 1; i < arr.length; i++) {
            char current = arr[i];
            int j = i - 1;
            while (j >= 0 && arr[j] > current) {
                arr[j + 1] = arr[j];
                j--;
            }
            arr[j + 1] = current;
        }
    }

    public static void main(String[] args) {
        char[] array = {'b', 'd', 'a', 'c', 'e'};
        System.out.println("Original Array: " + new String(array));

        // Selection Sort
        char[] selectionSortedArray = array.clone();
        selectionSort(selectionSortedArray);
        System.out.println("Selection Sorted: " + new String(selectionSortedArray));

        // Bubble Sort
        char[] bubbleSortedArray = array.clone();
        bubbleSort(bubbleSortedArray);
        System.out.println("Bubble Sorted: " + new String(bubbleSortedArray));

        // Insertion Sort
        char[] insertionSortedArray = array.clone();
        insertionSort(insertionSortedArray);
        System.out.println("Insertion Sorted: " + new String(insertionSortedArray));
    }
}
