# Task 1

## Description

Implement sorting algorithms for an array of characters in Java. The specific algorithms to be implemented are:

1. Direct Selection Sort
2. Bubble Sort
3. Insertion Sort

The array of characters is defined as follows:

```java
char[] array = new char[]{ /* ... character elements ... */ };
